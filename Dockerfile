FROM ubuntu

# Our Service Runs On Port 5000 tcp
EXPOSE 5000

# Get Runtime Deps + Build Deps For pyodbc
RUN apt-get update
RUN apt-get install -y apt-transport-https locales curl python3 python3-venv python3-dev build-essential unixodbc unixodbc-dev nginx openssl ca-certificates

# Set Out Locale For Python
RUN locale-gen en_US.UTF-8
RUN update-locale LANG=en_US.UTF-8

# Add Microsoft PPA And Install Driver For SQLServer
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update
RUN ACCEPT_EULA=Y apt-get install -y msodbcsql

# Setup The venv
RUN python3 -m venv /opt/timeclock-http-bridge/venv
RUN /opt/timeclock-http-bridge/venv/bin/pip install --upgrade pip
RUN /opt/timeclock-http-bridge/venv/bin/pip install flask flask-httpauth pyodbc gunicorn

# Copy Certificate And Related Config
COPY extras/cert/nginx-ssl.conf /etc/nginx/snippets
COPY extras/cert/nginx-secure-ssl-params.conf /etc/nginx/snippets
COPY extras/cert/timeclock-http-bridge.crt /opt/timeclock-http-bridge/
COPY extras/cert/timeclock-http-bridge.key /opt/timeclock-http-bridge/
COPY extras/cert/dhparam.pem /opt/timeclock-http-bridge/

# Clean Build Dependencies
RUN apt-get purge -y curl python3-dev build-essential unixodbc-dev
RUN apt-get autoremove -y
RUN apt-get clean

# Import And Enable Nginx Site
COPY extras/timeclock-http-bridge-nginx /etc/nginx/sites-available
RUN ln -s /etc/nginx/sites-available/timeclock-http-bridge-nginx /etc/nginx/sites-enabled

# Copy In Project And Run
COPY timeclock-http-bridge.py /opt/timeclock-http-bridge/
RUN chmod +x /opt/timeclock-http-bridge/timeclock-http-bridge.py
CMD nginx && /opt/timeclock-http-bridge/venv/bin/gunicorn --chdir /opt/timeclock-http-bridge/ --workers=4  --bind unix:/var/run/timeclock-http-bridge.sock timeclock-http-bridge:app