#!venv/bin/python

from datetime import datetime
from functools import wraps

from flask import Flask, g, abort, jsonify, request, make_response
from flask_httpauth import HTTPTokenAuth
import pyodbc
import os

app = Flask(__name__)
auth = HTTPTokenAuth(scheme='Token')

readonly_tokens = {
    "_rope%hulu _-queen3*TOKYO3visa%USA6coffee": "beckerlab_site_api"
}

readwrite_tokens = {
    "gfMU3eVQ6Eu5bLGT9LRtTeAksbqpdqfwzHVHhsJdenL2UckBpVdTzAnbHUqa3p5M": "punch-o-matic"
}


@auth.verify_token
def verify_token(token: str) -> bool:
    """
    Verifies a passed token is valid. Sets:
    g.current_user To the user/agent of the token
    g.can_write To True if this connection has write permissions

    Parameters
    ----------
    token: str
        The token from the Authorization header

    Returns
    -------
    bool
        If the auth token is valid or not

    """
    if token in readonly_tokens:
        g.current_user = readonly_tokens[token]
        g.can_write = False
        return True
    elif token in readwrite_tokens:
        g.current_user = readwrite_tokens[token]
        g.can_write = True
        return True
    return False


def require_readwrite(f):
    """
    Requires the connection have readwrite permissions to run the function below
    Be sure to decorate after auth.login_required
    """
    @wraps(f)
    def check_readwrite(*args, **kwargs):
        if not g.can_write:
            return make_response(jsonify({"Error": "Connection is readonly, but endpoint is readwrite"}), 403)
        return f(*args, **kwargs)
    return check_readwrite


def make_conn():
    """
    Produces a connection to the Timeclock database.
    Reads host from environment variable 'TIMECLOCK_DB_HOST'
    Reads port from environment variable 'TIMECLOCK_DB_PORT'
    Reads username from environment variable 'TIMECLOCK_DB_USER'
    Reads password from environment variable 'TIMECLOCK_DB_PASSWORD'

    Returns
    -------
    Any
        A configured connection object

    Raises
    ------
    EnvironmentError
        Either TIMECLOCK_DB_HOST, TIMECLOCK_DB_PORT, TIMECLOCK_DB_USER or TIMECLOCK_DB_PASSWORD environment variables are undefined
    """
    host = os.environ.get('TIMECLOCK_DB_HOST')
    if host is None:
        raise EnvironmentError("TIMECLOCK_DB_HOST Environment Variable Not Defined")

    port = os.environ.get('TIMECLOCK_DB_PORT')
    if port is None:
        raise EnvironmentError("TIMECLOCK_DB_PORT Environment Variable Not Defined")

    user = os.environ.get('TIMECLOCK_DB_USER')
    if user is None:
        raise EnvironmentError("TIMECLOCK_DB_USER Environment Variable Not Defined")

    password = os.environ.get('TIMECLOCK_DB_PASSWORD')
    if password is None:
        raise EnvironmentError("TIMECLOCK_DB_PASSWORD Environment Variable Not Defined")

    return pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};'
                          'SERVER=tcp:' + host + ',' + port + ';'
                          'DATABASE=TimeClockPlus;'
                          'UID=' + user + ';PWD=' + password)


def employee_exists(employee_id: int) -> bool:
    """
    Tests if employee_id is the ID of an existing employee

    Parameters
    ----------
    employee_id: int
        The ID of the employee to search for

    Returns
    -------
    bool
        True If the employee with ID employee_id was found.
        False otherwise
    """
    conn = make_conn()
    cur = conn.cursor()
    cur.execute("SELECT COUNT(EmployeeId) AS 'EmployeeExists' FROM EmployeeList WHERE EmployeeId=?", [employee_id])
    return cur.fetchone().EmployeeExists == 1


def is_clocked_in(employee_id: int) -> bool:
    """

    Parameters
    ----------
    employee_id: int
        The ID of the employee to search for an open punch

    Returns
    -------
    bool
        True If the employee with ID employee_id is clocked in.
        False otherwise

    """
    conn = make_conn()
    cur = conn.cursor()
    cur.execute("SELECT COUNT(RecordId) AS 'EmployeeIn' FROM EmployeeHours"
                " WHERE EmployeeId=? AND TimeOut IS NULL",
                [employee_id])
    return cur.fetchone().EmployeeIn == 1


def get_http_exception_handler(flask_app):
    """Overrides the default http exception handler to return JSON."""
    handle_http_exception = flask_app.handle_http_exception

    @wraps(handle_http_exception)
    def ret_val(exception):
        exc = handle_http_exception(exception)
        return jsonify({'code': exc.code, 'message': exc.description}), exc.code
    return ret_val


# Override the HTTP exception handler.
app.handle_http_exception = get_http_exception_handler(app)


@app.errorhandler(401)
def unauthorized(error):
    return make_response(jsonify({'error': 'Unauthorized'}), 401)


@app.errorhandler(403)
def unauthorized(error):
    return make_response(jsonify({'error': 'Forbidden'}), 403)


@app.route('/')
@auth.login_required
def index():
    return "Hello, World!"


@app.route('/employee', methods=['GET'])
@auth.login_required
def get_employees():
    """
    Retrieves all employees in the specified department

    Accepts the following GET parameters:

    active_on str (optional): Set to retrieve employees with termination dates (DateLeft) after this value,
    or no termination date at all. (i.e. Employees active on, and after this date)
    formatted mm-dd-yyyy Inclusive
    Default Value '10-19-1995'

    department str: Name of the department to list the employees of. Default 'CIS DEPT'

    Returns
    -------
    Any
        200: JSON Response of employees as an array with
        EmployeeId, FirstName, LastName, DateHire, DateLeft
        properties
    """
    department = request.args.get('department', default='CIS DEPT', type=str)
    req_active_on = request.args.get('active_on', default=None, type=str)
    if req_active_on is not None:
        active_on = datetime.strptime(req_active_on, '%m-%d-%Y')
    else:
        # Set Well Before Any Data
        active_on = datetime.strptime('10-19-1995', '%m-%d-%Y')

    conn = make_conn()
    cur = conn.cursor()
    cur.execute(
        "SELECT EmployeeId, FirstName, LastName, DateHire, DateLeft"
        " FROM TimeClockPlus.dbo.EmployeeList WHERE Department=?"
        "  AND (DateLeft > ? OR DateLeft IS NULL)"
        " ORDER BY LastName",
        [department, active_on])

    db_results = cur.fetchall()
    converted_results = []

    for row in db_results:
        result = {'EmployeeId': int(row.EmployeeId), 'FirstName': row.FirstName, 'LastName': row.LastName}

        if row.DateHire is None:
            result['DateHire'] = None
        else:
            result['DateHire'] = row.DateHire.strftime('%m-%d-%Y %H:%M:%S')

        if row.DateLeft is None:
            result['DateLeft'] = None
        else:
            result['DateLeft'] = row.DateLeft.strftime('%m-%d-%Y %H:%M:%S')

        converted_results.append(result)

    return jsonify(converted_results)


@app.route('/punch', methods=['GET'])
@auth.login_required
def get_employee_hours():
    """
    Retrieves punches within a date range.

    Accepts the following GET parameters:

    employee_id int: (optional) ID of the employee to retrieve hours for.
    If not found 404 will be returned

    start str (optional): Day to start the result range at
    formatted mm-dd-yyyy Inclusive
    Default Value '10-19-1995'

    end str (optional): Day to end the result range at
    formatted mm-dd-yyyy Inclusive
    Default Value Current Date

    department str (optional): Department To Pull Employees From
    Default Value 'CIS DEPT'

    Returns
    -------
    Any
        200:JSON Response of employee hours as an array with
        TimeIn,TimeOut properties.

        404: Employee specified by employee_id was not found
    """
    parameters = {}

    employee_id = request.args.get('employee_id', default=None, type=int)
    if employee_id is not None:
        parameters['employee_id'] = employee_id

    req_start = request.args.get('start', default=None, type=str)
    if req_start is not None:
        parameters['start_date'] = datetime.strptime(req_start, '%m-%d-%Y')
    else:
        # Set Well Before Any Data
        parameters['start_date'] = datetime.strptime('10-19-1995', '%m-%d-%Y')

    req_end = request.args.get('end', default=None, type=str)
    if req_end is not None:
        parameters['end_date'] = datetime.strptime(req_end, "%m-%d-%Y").replace(hour=23, minute=59)
    else:
        parameters['end_date'] = datetime.now()

    req_dept = request.args.get('department', None, str)
    if req_dept is None:
        parameters['department'] = 'CIS DEPT'
    else:
        parameters['department'] = req_dept

    conn = make_conn()
    cur = conn.cursor()

    # Verify Employee Exists Only If Punches Were Requested For A Specific One
    if 'employee_id' in parameters:
        if not employee_exists(employee_id):
            return make_response(jsonify({'error': 'Employee not found'}), 404)

        # Employee Exists, Return Punches
        cur.execute("SELECT EmployeeHours.RecordId AS 'RecordId', EmployeeHours.EmployeeId AS 'EmployeeId',"
                    " TimeIn, TimeOut "
                    "FROM EmployeeHours "
                    "INNER JOIN EmployeeList ON EmployeeHours.EmployeeId = EmployeeList.EmployeeId "
                    "WHERE EmployeeHours.EmployeeId=? "
                    "  AND TimeIn > ? "
                    "  AND TimeOut < ? "
                    "  AND EmployeeList.Department = ? "
                    "ORDER BY TimeIn, EmployeeId",
                    [int(parameters['employee_id']),
                     parameters['start_date'],
                     parameters['end_date'],
                     parameters['department']])
    else:
        cur.execute("SELECT EmployeeHours.RecordId AS 'RecordId', EmployeeHours.EmployeeId AS 'EmployeeId'"
                    ",TimeIn, TimeOut "
                    "FROM EmployeeHours "
                    "INNER JOIN EmployeeList ON EmployeeHours.EmployeeId = EmployeeList.EmployeeId "
                    "WHERE TimeIn > ? "
                    "  AND TimeOut < ? "
                    "  AND EmployeeList.Department = ? "
                    "ORDER BY TimeIn, EmployeeId",
                    [parameters['start_date'], parameters['end_date'], parameters['department']])

    db_results = cur.fetchall()
    converted_results = []
    for row in db_results:
        result = {'RecordId': int(row.RecordId),
                  'EmployeeId': int(row.EmployeeId),
                  'TimeIn': row.TimeIn.strftime('%m-%d-%Y %H:%M:%S')}
        if row.TimeOut is None:
            result['TimeOut'] = None
        else:
            result['TimeOut'] = row.TimeOut.strftime('%m-%d-%Y %H:%M:%S')
        converted_results.append(result)

    return jsonify(converted_results)


@app.route('/punch', methods=['PUT'])
@auth.login_required
@require_readwrite
def insert_punch():
    parameters = {}

    employee_id = int(request.json.get("EmployeeId"))
    if not employee_exists(employee_id):
        return make_response(jsonify({'error': 'Employee not found'}), 404)
    parameters['employee_id'] = employee_id

    if request.json.get("TimeIn") is None:
        return make_response(jsonify({"Error": "TimeIn Not Defined"}), 400)
    parameters['time_in'] = datetime.strptime(request.json.get("TimeIn") , '%m-%d-%Y %H:%M:%S')

    if request.json.get("TimeOut") is None:
        if is_clocked_in(employee_id):
            return make_response(jsonify({"Error": "Employee Already Clocked In"}), 409)
        parameters['time_out'] = None
    else:
        parameters['time_out'] = datetime.strptime(request.json.get("TimeOut"), '%m-%d-%Y %H:%M:%S')

    conn = make_conn()
    cur = conn.cursor()

    cur.execute("INSERT INTO [TimeClockPlus].[dbo].[EmployeeHours]"
                "(EmployeeId, Company, TimeIn, TimeOut,"
                "JobCode, CostCode, Rate, Tracked1, Tracked2, Tracked3,"
                "BreakFlag, ManagerApproval, EmployeeApproval, OtherApproval,"
                "InVariance, OutVariance, Flag, SDPremium,"
                "InLocationID, OutLocationID,"
                "MissedInPunch, MissedOutPunch, MissedOutApproval,"
                "TimeSheetMinutes, UTCDateAdded, UTCDateModified,"
                "MergeInId, MergeOutId, TZOffset,"
                "TeacherId, TimeOffset,"
                "EmployeeApprovalUID, ManagerApprovalUID, OtherApprovalUID) "
                "VALUES "
                "(?, 100, ?, ?,"
                "1, '', 0, NULL, NULL, NULL,"
                "0, 0, 0, 0,"
                "0, 0, 4, NULL,"
                "8022, 8022,"
                "0, 0, 0,"
                "NULL, GETUTCDATE(), GETUTCDATE(),"
                "NULL, NULL, NULL,"
                "NULL, 0,"
                "NULL, NULL, NULL);", [parameters['employee_id'], parameters['time_in'], parameters['time_out']])
    cur.commit()
    cur.execute("SELECT @@IDENTITY AS id")
    res = cur.fetchone()
    return make_response(jsonify({"id": int(res.id)}), 201)


@app.route('/punch/open', methods=['GET'])
@auth.login_required
def get_open_punches():
    employee_id = request.args.get('employee_id', default=None, type=int)
    conn = make_conn()
    cur = conn.cursor()

    if employee_id is not None:
        if not employee_exists(employee_id):
            return make_response(jsonify({"Error": "Employee Not Found"}), 404)
        cur.execute("SELECT RecordId, EmployeeId, TimeIn, TimeOut FROM EmployeeHours"
                    " WHERE EmployeeId=? AND TimeOut IS NULL"
                    " ORDER BY TimeIn",
                    [employee_id])
    else:
        cur.execute("SELECT RecordId, EmployeeId, TimeIn, TimeOut FROM EmployeeHours"
                    " WHERE TimeOut IS NULL"
                    " ORDER BY TimeIn", [])
    db_results = cur.fetchall()
    converted_results = []
    for row in db_results:
        converted_results.append({'RecordId': int(row.RecordId),
                                  'EmployeeId': int(row.EmployeeId),
                                  'TimeIn': row.TimeIn.strftime('%m-%d-%Y %H:%M:%S'),
                                  'TimeOut': None})
    return jsonify(converted_results)


@app.route('/punch/<int:record_id>', methods=['POST'])
@auth.login_required
@require_readwrite
def update_punch(record_id):
    conn = make_conn()
    cur = conn.cursor()

    cur.execute("SELECT EmployeeId, RecordId, EmployeeId, TimeIn, TimeOut FROM EmployeeHours"
                " WHERE RecordId=?", record_id)
    res = cur.fetchone()

    if res is None:
        return make_response(jsonify({"Error", "Punch not found"}))

    if 'TimeIn' in request.json:
        time_in = datetime.strptime(request.json.get("TimeIn"), '%m-%d-%Y %H:%M:%S')
    elif res.TimeIn is not None:
        time_in = res.TimeIn
    else:
        return make_response(jsonify({"Error": "TimeIn Is Required"}), 400)

    if 'TimeOut' in request.json and request.json.get('TimeOut') is None:
        if is_clocked_in(res.EmployeeId):
            return make_response(jsonify({"Error": "Employee Already Clocked In"}), 409)

        time_out = None
    elif request.json.get('TimeOut') is not None:
        time_out = datetime.strptime(request.json.get('TimeOut'), '%m-%d-%Y %H:%M:%S')
    else:
        time_out = res.TimeOut

    cur.execute("UPDATE EmployeeHours SET TimeIn=? , TimeOut=? WHERE RecordId=?", [time_in, time_out, record_id])
    cur.commit()

    return make_response('', 204)


# This Is The Development Binding
# See The DockerFile For The Production Binding
if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5000)
