# TimeClock API

Welcome to the TimeClock API. This page documents the endpoints and the data
they return

## Example

 All endpoint documentation will follow this template
 
### /path/(in_url_param)/resource

#### Method: GET

#### Permission: readonly/readwrite

 Specifies the path to the endpoint off of the root. As well as the HTTP method
 
 (in_url_param): Is a required parameter encoded directly into the URL. e.g. `/path/111/resource`
 
#### Parameters

##### Parameter List

<dl>
    <dt>in_url_param</dt>
    <dd>Documentation for the above parameter. This parameter is required</dd>
    <dt>[optional_param]</dt>
    <dd>Optional parameters are in square braces [].</dd>
    <dt>required_qs_param</dt>
    <dd>A required parameter not in the URL</dd>
</dl> 

##### Passing Parameters

Depending on the Method, parameters will pe passed in different ways.

* GET: Parameters are pass as query string arguments. e.g. `(url)?param_1='value'&the_rest='value'`
* POST, PUT: Data should be passed as JSON. e.g. `{"name":"Evan", ....}`
* DELETE: Parameters should only be directly in the URL. No query string or JSON

#### REQUEST:
This section shows what an example request would look like.
(base_address):(port) are the address of the server and port in which the TimeClock API is listening Default: 5000
Most times, all optional parameters are shown.
The HTTP method will appear before the URL. In The case of POST/PUT, data will be shown afterward.


`
GET (base_address):(port)/path/(in_url_param)/resource?required_qs_param='value1'&optional_param='value2'
`

#### RESPONSE:
This section shows how the server would reply, assuming the request succeeded.
The data will be JSON except when the HTTP response code is 204 (No Content), in
Which case no data would be returned.

This is an example JSON return of punches
```json
[
  {
    "EmployeeId": 2288, 
    "TimeIn": "02-22-2018 13:54:00", 
    "TimeOut": "02-22-2018 15:09:00"
  }, 
  {
    "EmployeeId": 8284, 
    "TimeIn": "02-22-2018 14:55:00", 
    "TimeOut": "02-22-2018 15:55:00"
  }
]
```

# Authorization

All requests to the TimeClock API must be authorized, unless stated otherwise. Every request must have
the Authorization HTTP header set (see: [MDN](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization))

API tokens are shared in advance and do not change.

Tokens may be readonly or read-write. The authorization of each should be obvious.

If a request is made without the Authorization set, or it is set to an invalid value, HTTP 401/403 will be returned.

If a write request is sent to a write endpoint with a readonly token, HTTP 403 will be returned with

```json
{
  "Error": "Connection is readonly, but endpoint is readwrite"
}
```


## Header

The header should be set like the following:

`
Authorization: Token swordfish
`

<dl>
    <dt>Authorization</dt>
    <dd>The name of the header</dd>
    <dt>Token</dt>
    <dd>Name of the authorization type. Token is currently the only supported type.</dd>
    <dt>swordfish</dt>
    <dd>Value of your API Token. (swordfish is just an example)</dd>
</dl> 


# Environment

The application is primarily configured through the environment variables defined here.
All are required unless specified otherwise

<dl>
    <dt>TIMECLOCK_DB_HOST</dt>
    <dd>The hostname/ip address of the SQLServer database to connect to. Connection is through TCP</dd>
    <dt>TIMECLOCK_DB_PORT</dt>
    <dd>The TCP port of the SQLServer database to connect to.</dd>
    <dt>TIMECLOCK_DB_USER</dt>
    <dd>The SQLServer user to connect as</dd>
    <dt>TIMECLOCK_DB_PASSWORD</dt>
    <dd>The password for the user defined by TIMECLOCK_DB_USER</dd>
</dl> 


# Endpoints

## Employee

### /employee

#### Method: GET

#### Permission: readonly/readwrite


Returns an array of employee in a given department.
Results are ordered alphabetically by employee's last name.

#### Parameters

 
 <dl>
    <dt>[department]</dt>
    <dd>Department to show the employees of. Default is 'CIS DEPT'</dd>
    <dt>[active_on]</dt>
    <dd>active_on str (optional): Set to retrieve employees with termination dates (DateLeft) after this value,
    or no termination date at all. (i.e. Employees active on, and after this date)
    formatted mm-dd-yyyy Inclusive
    Default Value '10-19-1995'</dd>
</dl>


#### REQUEST:
`
GET (base_address):(port)/employee?department='CIS DEPT'&active_on=10-19-1995
`
#### RESPONSE:
```json
[
  {
    "DateHire": "05-08-2015 00:00:00", 
    "DateLeft": null, 
    "EmployeeId": 8267, 
    "FirstName": "Evan", 
    "LastName": "Black"
  }, 
  {
    "DateHire": "01-01-2016 00:00:00", 
    "DateLeft": null, 
    "EmployeeId": 8270, 
    "FirstName": "Chris", 
    "LastName": "Frye"
  }, 
  {
    "DateHire": "01-01-2016 00:00:00", 
    "DateLeft": null, 
    "EmployeeId": 8272, 
    "FirstName": "Liz", 
    "LastName": "Stanton"
  },
  {
    "DateHire": "08-25-2017 00:00:00", 
    "DateLeft": null, 
    "EmployeeId": 8283, 
    "FirstName": "John", 
    "LastName": "Georgvich"
  }, 
  {
    "DateHire": "01-22-2018 00:00:00", 
    "DateLeft": null, 
    "EmployeeId": 8287, 
    "FirstName": "Ethan", 
    "LastName": "Dyer"
  }
]

```

## Punch

### /punch

#### Method: GET

#### Permission: readonly/readwrite

Returns an array of punches. Optionally for an employee and/or date range.
Punches are ordered by TimeIn.

#### Parameters


<dl>
    <dt>[employee_id]</dt>
    <dd>The ID of the employee to retrieve hours for. <br />
Returns 404 If this employee is not found. <br />
If this option is unspecified, then all punches within the range will be returned.</dd>
    <dt>[department]</dt>
    <dd>Department to pull employees from. Default Value 'CIS DEPT'</dd>
    <dt>[start]</dt>
    <dd>Day to start the result range at, includes the day specified, formatted `mm-dd-yyyy`. Default Value '10-19-1995'</dd>
    <dt>[end]</dt>
    <dd>Day to end the result range at, includes the day specified, formatted `mm-dd-yyyy`. Default Value Current Date</dd>
</dl>


#### REQUEST:
`
GET (base_address):(port)/punch?employee_id=8267&start='05-01-2015'&end='05-09-2018'
`
#### RESPONSE:
Success, Several employees HTTP 200 (Results shortened)
```json
[
  {
    "EmployeeId": 8277, 
    "RecordId": 69970,
    "TimeIn": "02-22-2018 12:54:00", 
    "TimeOut": "02-22-2018 13:55:00"
  }, 
  {
    "EmployeeId": 2288, 
    "RecordId": 69977,
    "TimeIn": "02-22-2018 13:54:00", 
    "TimeOut": "02-22-2018 15:09:00"
  }, 
  {
    "EmployeeId": 8284, 
    "RecordId": 69980,
    "TimeIn": "02-22-2018 14:55:00", 
    "TimeOut": "02-22-2018 15:55:00"
  }

]
```

---

### /punch

#### Method: PUT

#### Permission: readwrite

Inserts a new punch for a given employee.

#### Parameters

<dl>
    <dt>[EmployeeId]</dt>
    <dd>The ID of the employee to attach the punch to <br />
Returns 404 If this employee is not found.</dd>
    <dt>TimeIn</dt>
    <dd>Time the employee clocked in, formatted `mm-dd-yyyy HH:MM:SS`. Example '10-19-2018 13:55:00'</dd>
    <dt>[TimeOut]</dt>
    <dd>Time the employee clocked out, if unspecified the shift is left open/in progress. May also be explicitly set to null. <br />
    Will return HTTP 409 if trying to add another open punch when one already exists for the given employee <br />
    Formatted `mm-dd-yyyy HH:MM:SS`. Example '10-19-2018 13:55:00'</dd>
</dl>

#### REQUEST:

`PUT (base_address):(port)/punch`

Request JSON (Open Punch)
```json
{
  "EmployeeId": 8267,
  "TimeIn":"02-27-2018 13:55:00",
  "TimeOut":null
}
```

Request JSON (Closed Punch)
```json
{
  "EmployeeId": 8267,
  "TimeIn": "02-27-2018 13:55:00",
  "TimeOut": "02-27-2018 14:55:00"
}
```

#### RESPONSE:
Success (HTTP 200)
```json
{
  "id": 69990
}
```

Trying to add a punch with no TimeIn HTTP 400

```json
{
  "Error": "TimeIn Not Defined"
}
```


Trying to add an open punch when one exists for that employee (HTTP 409)
```json
{
  "Error": "Employee Already Clocked In"
}
```

Nonexistent employee (HTTP 404)
```json
{
  "error": "Employee not found"
}
```

---

### /punch/(PunchId)

#### Method: POST

#### Permission: readwrite

Updates a given punch

#### Parameters

<dl>
    <dt>TimeIn</dt>
    <dd>Time the employee clocked in, formatted `mm-dd-yyyy HH:MM:SS`. Example '10-19-2018 13:55:00'</dd>
    <dt>[TimeOut]</dt>
    <dd>Time the employee clocked out, if unspecified the shift is left open/in progress. May also be explicitly set to null. <br />
    Will return HTTP 409 if trying to edit a punch to be open when another open punch already exists for the given employee <br />
    Formatted `mm-dd-yyyy HH:MM:SS`. Example '10-19-2018 13:55:00'/dd>
</dl>

#### REQUEST:

`POST  (base_address):(port)/punch/(PunchId)`


Request JSON (Open Punch)
```json
{
  "TimeIn":"02-27-2018 13:55:00",
  "TimeOut":null
}
```

Request JSON (Closed Punch)
```json
{
  "TimeIn": "02-27-2018 13:55:00",
  "TimeOut": "02-27-2018 14:55:00"
}
```

#### RESPONSE:
Success (HTTP 204) No Content
```
(Empty)
```

Trying to edit a punch with no TimeIn HTTP 400

```json
{
  "Error": "TimeIn Is Required"
}
```

Trying to add an open punch when one exists for that employee (HTTP 409)
```json
{
  "Error": "Employee Already Clocked In"
}
```

Nonexistent employee (HTTP 404)
```json
{
  "error": "Employee not found"
}
```

---

### /punch/open

#### Method: GET

#### Permission: readonly/readwrite

Returns an array of currently open punches. Optionally for an employee.
Punches are ordered by TimeIn.

#### Parameters

<dl>
    <dt>[EmployeeId]</dt>
    <dd>The ID of the employee to search for open punches <br />
Returns 404 If this employee is not found.</dd>
</dl>

#### REQUEST:

Base:

`GET (base_address):(port)/punch/open`


For Employee:

`GET (base_address):(port)/punch/open?employee_id=8267`

#### RESPONSE

All employees HTTP 200
```json
[
  {
    "EmployeeId": 8281, 
    "RecordId": 69981, 
    "TimeIn": "02-22-2018 15:55:00", 
    "TimeOut": null
  }, 
  {
    "EmployeeId": 2728, 
    "RecordId": 69982, 
    "TimeIn": "02-22-2018 15:58:00", 
    "TimeOut": null
  }
]

```

One employee HTTP 200
```json
[
  {
    "EmployeeId": 8281, 
    "RecordId": 69981, 
    "TimeIn": "02-22-2018 15:55:00", 
    "TimeOut": null
  }
]
```

One employee no open HTTP 200

```json
[]
```

Employee not found HTTP 404
```json
{
  "Error": "Employee Not Found"
}
```

---