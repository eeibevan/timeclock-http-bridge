# Deployment

## Docker
timeclock-http-bridge May be deployed as a docker image.

### Networking
The timeclock-http-bridge will either have to be connected to an outside
TimeClock database, or connected to a separate Docker container running 
the database.

The example uses a [Docker Network](https://docs.docker.com/network/)
To connect to the container running the database.

## SSL Certificate
The Docker configuration requires an SSL certificate.

When the image is built, the certificate is copied from the `extras/cert/timeclock-http-bridge.[crt/key]`
along with the Diffie-Hellman file `dhparam.pem`

These files are stored in `/opt/timeclock-http-bridge` in the container

If you are building the image and do not have an SSL certificate to include
you may generate a self-signed one by running the following script in `extras/cert`.

```bash
./generate_selfsigned_cert.sh
```

### Example
This is an example of a run configuration for a timeclock-http-bridge container.

See [Environment Documentation](index.md#environment) for an explanation of each of the `-e` switches.
The rest will be examined in the following section
```bash
docker run --publish 5001:5000 \
    --name timeclock-http-bridge \
    --tty \
    --detach \
    --network=timeclock-network \
    -e TIMECLOCK_DB_HOST="timeclockdb" \
    -e TIMECLOCK_DB_PORT="1433" \
    -e TIMECLOCK_DB_USER="sa" \
    -e TIMECLOCK_DB_PASSWORD=[PASSWORD] \
    registry.gitlab.com/clarion-fsr/timeclock-http-bridge
```


### Explanation
This is not the [Docker Documentation](https://docs.docker.com/)
but here is a brief overview of the above command.

#### Publish
`--publish` Exposes a given port to the outside world.
Here we expose port 5001 and link it to the container's port 5000

`--publish 5001:5000 `

#### Name
See: [Run - name](https://docs.docker.com/engine/reference/run/#name-name)

`--name` Defines the name of the running container. Names are unique, and
the container is usually referenced by name rather then by id.

`--name timeclock-http-bridge`

#### Network
See: [Docker Network](https://docs.docker.com/network/)

Note the section of the command:

`--network=timeclock-network `

This network should be the same network the database container is running on.
If the TimeClock Database does not run in a docker container, then the `--network`
option is not needed.

Docker networks can be created with the following command:
```bash
docker network create NETWORK-NAME
```

Docker containers may be attached and detached from networks even after they are created:

```bash
# Attach container timeclock-http-bridge to timeclock-network
docker network connect timeclock-network timeclock-http-bridge

# Detach container timeclock-http-bridge from timeclock-network
docker network disconnect timeclock-network timeclock-http-bridge
```

Of course, they may also be attached when the container is run, as shown above.

#### TIMECLOCK_DB_HOST

Docker containers attached to the same network will be able to resolve each other by
container name, as well as IP address.

Note the environment variables from the example:

`TIMECLOCK_DB_HOST="timeclockdb"`

timeclockdb Is the name of another Docker container running the database.

#### TIMECLOCK_DB_PORT

Note the port number is not the port published by the default timeclockdb Docker Image,
but rather SQLServer's own default port. The internal ports of the containers are used when they
are networked, rather then their outside ones

`TIMECLOCK_DB_PORT="1433"`
