#!/bin/bash
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout timeclock-http-bridge.key -out timeclock-http-bridge.crt -subj "/C=US/ST=Pennsylvania/L=Clarion/O=Clarion University/CN=Evan Black/emailAddress=beckerlab@clarion.edu"
openssl dhparam -out dhparam.pem 2048
