#!/bin/bash

apt-get update
apt-get install -y apt-transport-https locales curl

locale-gen en_US.UTF-8
update-locale LANG=en_US.UTF-8

curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -

#Ubuntu 16.04
curl https://packages.microsoft.com/config/ubuntu/16.04/prod.list > /etc/apt/sources.list.d/mssql-release.list

apt-get update

ACCEPT_EULA=Y apt-get install -y msodbcsql
