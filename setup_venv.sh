#!/bin/bash

# Get Base Dependencies
if [ "$EUID" -eq 0 ]; then
    apt-get install -y python3 python3-venv python3-dev build-essential unixodbc unixodbc-dev
else
    sudo apt-get install -y python3 python3-venv python3-dev build-essential unixodbc unixodbc-dev
fi

# Create Virtual Environment
python3 -m venv ./venv

# Make Sure pip Is Up To Date
./venv/bin/pip install --upgrade pip

# Pull Python Dependencies
./venv/bin/pip install flask flask-httpauth pyodbc gunicorn
